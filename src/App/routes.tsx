import React from "react";
import { Route, Switch } from "react-router-dom";

import NoMatch from "./containers/404/component";
import JobsList from './containers/JobsList/';
import OneJob from "./containers/OneJob/";
import NewJob from './containers/NewJob';
import UpdateJob from './containers/UpdateJob';

import App from './App';

export default () => (
  <Switch>
    <Route exact={true} path='/pms' component={App} />
    <Route exact={true} path='/pms/jobs' component={JobsList} />
    <Route exact={true} path='/pms/jobs/new' component={NewJob} />
    <Route exact={true} path='/pms/jobs/:id/edit' component={UpdateJob} />
    <Route exact={true} path='/pms/jobs/:id' component={OneJob} />
    <Route component={NoMatch} />
  </Switch>
);
