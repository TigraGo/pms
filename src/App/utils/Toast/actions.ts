import { IAction } from "../../../models";
import { ActionCreator } from "redux";

export const SET_TOAST: string = "SET_TOAST";
export const REMOVE_TOAST: string = "REMOVE_TOAST";

export const setToast: ActionCreator<IAction> = (variant, isOpen, header, message) => ({
    type: SET_TOAST,
    payload :{
        variant: variant,
        isOpen: isOpen,
        header: header,
        message: message,
    }
});

export const removeToast: ActionCreator<any> = (timeout: number) => async (dispatch: any) => {
    setTimeout(() => {
        dispatch(setToast('', false, '', ''))
    }, timeout);
}