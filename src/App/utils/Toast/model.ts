export interface IInitialState {
    toast: {
        isOpen: boolean;
        variant: string;
        header: string;
        message: string;
    }
}