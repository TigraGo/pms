import {setToast, removeToast} from './actions';
import { Dispatch } from 'redux';

const toast = (typeMessage: string = 'info', header: string, message: string, timeout: number = 4000) => async (dispatch: Dispatch) => {
  dispatch(setToast(typeMessage, true, header, message));
  await dispatch(removeToast(timeout));
};

export const successToast = (title: string, message: string, timeout?: number) => async (dispatch: any) => {
  await dispatch(toast('success', title, message, timeout));
}

export const errorToast = (title: string, message: string, timeout?: number) => async (dispatch: any) => {
  await dispatch(toast('error', title, message, timeout));
}

export const warningToast = (title: string, message: string, timeout?: number) => async (dispatch: any) => {
  await dispatch(toast('warning', title, message, timeout));
}

export const infoToast = (title: string, message: string, timeout?: number) => async (dispatch: any) => {
  await dispatch(toast('info', title, message, timeout));
}