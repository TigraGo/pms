import {
    SET_TOAST
  } from "./actions";
  import { IInitialState } from "./model";
  import { IAction } from "../../../models";
  
  export const initialState: IInitialState = {
    toast: {
      isOpen: false,
      variant: 'default',
      header: '',
      message: ''
    }
  };
  
  export const getToastSelector = (state: any) => state.toast.toast;
  
  export default (state = initialState, action: IAction) => {
    switch (action.type) {
      case SET_TOAST:
        return {
          ...state,
          toast: action.payload
        }
      default:
        return state;
    }
  };
  