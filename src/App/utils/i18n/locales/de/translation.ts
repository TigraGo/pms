const deTranslation: any = {
  "listjob": {
    "type": "Typ",
    "site": "Hotel",
    "createnewjob": "Einen neuen Job anlegen",
    "nojobsyet": "Es gibt noch keine Jobs."
  },
  "onejob": {
    "vendor": "Hersteller",
    "credentials": "Anmeldedaten",
    "_type": "_type",
    "username": "Benutzername",
    "password": "Passwort",
    "protocol": "Protokol",
    "host": "Host",
    "port": "Port",
    "path": "Pfad",
    "domain": "Domain",
    "share": "Freigabe",
    "executions": "Ausführungen",
    "selectstatus": "Status auswählen",
    "filter": "Filter",
    "status": "Status",
    "startdate": "Startdatum",
    "enddate": "Enddatum",
    "errormessage": "Fehlermeldung",
    "files": "Dateien",
    "none": "Keiner",
    "noexecutions": "Wir können keine Ausführungen für diesen Job finden :("
  },
  "editadd": {
    "choosecredentialstype": "Wählen Sie die Authentifizierungsmethode",
    "reset": "Zurücksetzen",
    "submit": "Speichern",
    "choosevendor": "Hersteller wählen"
  }
}

export default deTranslation;