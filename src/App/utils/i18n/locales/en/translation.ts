const enTranslation: any = {
  "listjob": {
    "type": "Type",
    "site": "Hotel",
    "createnewjob": "Add a new job",
    "nojobsyet": "There are no jobs, yet"
  },
  "onejob": {
    "vendor": "Vendor",
    "credentials": "Credentials",
    "_type": "_type",
    "username": "Username",
    "password": "Password",
    "protocol": "Protocol",
    "host": "Host",
    "port": "Port",
    "path": "Path",
    "domain": "Domain",
    "share": "Share",
    "executions": "Executions",
    "selectstatus": "Select the status",
    "filter": "Filter",
    "status": "Status",
    "startdate": "Start date",
    "enddate": "End date",
    "errormessage": "Error message",
    "files": "Files",
    "none": "None",
    "noexecutions": "We can't find any executions for this job 😦"
  },
  "editadd": {
    "choosecredentialstype": "Choose the authentication method",
    "reset": "Reset",
    "submit": "Save",
    "choosevendor": "Choose the vendor"
  }
}

export default enTranslation;