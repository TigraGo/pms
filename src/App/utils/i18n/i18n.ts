import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

// import LanguageDetector from 'i18next-browser-languagedetector';
import deTranslation from './locales/de/translation';
import enTranslation from './locales/en/translation';

i18n
  .use(initReactI18next)
  .init({
    lng: String(localStorage.getItem('language')) || 'en',
    fallbackLng: 'en',
    debug: true,

    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
    resources: {
      en: {
        translation: enTranslation
      },
      de: {
        translation: deTranslation
      }
    }
  });


export default i18n;