export interface IInitialState {
  error: {};
  data: {};
  loading: boolean;
}

export interface IProps {
  loading: boolean;
  toast: any;
  sendNewJob: (body: any) => void;
}

export interface IResponse {
  body: string;
  id: number;
  title: string;
  userId: number;
}