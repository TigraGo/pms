import { successToast, errorToast } from '../../utils/Toast/toast';


import {
  newJobError,
  newJobRequest,
  newJobSuccess,
} from "./actions";
import { API, api } from "../../../services/api";

export const sendNewJob: any = (body: any) => async (dispatch: any) => {
  dispatch(newJobRequest());
  try {
    const response = await api(`${API.JOBS}`, 'POST', body);

    if (response.status < 300) {
      dispatch(newJobSuccess());
      await dispatch(successToast('CREATE JOB', 'Job was successfully created'));
      return;
    }

    dispatch(newJobError());
    await dispatch(errorToast('CREATE JOB', `Error was found with code ${response.status}`));

  } catch (error) {
    dispatch(newJobError(error));
    dispatch(errorToast('CREATE JOB', 'Error was found'));
  }
};
