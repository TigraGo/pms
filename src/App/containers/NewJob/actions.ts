import {
  CREATE_JOB_ERROR,
  CREATE_JOB_REQUEST,
  CREATE_JOB_SUCCESS,
} from "./constants";
import { IAction } from "../../../models";
import { ActionCreator } from "redux";

export const newJobRequest: ActionCreator<IAction> = () => ({
  type: CREATE_JOB_REQUEST
});

export const newJobSuccess: ActionCreator<IAction> = payload => ({
  type: CREATE_JOB_SUCCESS,
  payload
});

export const newJobError: ActionCreator<IAction> = payload => ({
  type: CREATE_JOB_ERROR,
  payload
});