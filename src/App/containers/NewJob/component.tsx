import React, { FC, useState } from "react";
import {Form, Field} from "react-final-form";
import { Loader, Select, TextField, Button, Toast } from "@betterspace/react-component-suite";
import { pmsOptions, credentialOptions, SmbCredentials, MailCredentials, FtpCredentials } from "./constants";

import { IProps } from "./model";
import "./style.scss";
import { useTranslation } from "react-i18next";

const NewJob: FC<IProps> = ({
  loading,
  sendNewJob,
  toast
}) => {

  const [defaultSelectValue] = useState('');
  const { t } = useTranslation(); 
  const onSubmit = (values: any) => {
    let credentials: any = JSON.parse(JSON.stringify(values)); // clone new object for setting in 'credentials'
    credentials['_type'] = values._type.value; // removing unnecessary properties
    delete credentials['type']; // removing unnecessary properties
    delete credentials['site']; // removing unnecessary properties

    const body = { // summary object for request
      type: values.type.value,
      site: values.site,
      credentials
    }
    
    sendNewJob(JSON.stringify(body)); 
  }

  if (loading) {
    return (
      <div className="loading">
        <Loader width="25%" />
      </div>
    );
  }

  return (
    <div className="jobslist create-job">
      <Form
      onSubmit={onSubmit}
      initialValues={{ type: {value: "ubsofthotel50" }, _type: {value: "mail"} }}
      render={({ handleSubmit, form, submitting, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <h2>{t('editadd.choosecredentialstype')}</h2>
          <div>
            <Field name="type">
              {props => (
                <div>
                  <Select placeholder="Choose type" options={pmsOptions} isMulti={false} onChange={props.input.onChange} defaultValue={{ value: defaultSelectValue, label: "Choose credential type" }} />
                </div>
              )}
            </Field>
          </div>
          <div>
            <Field name="site">
              {props => (
                <TextField
                  id="site"
                  label="Site"
                  placeholder="Site"
                  onChange={props.input.onChange}
                  margin="normal"
                />
              )}
            </Field>
          </div>
          <div style={{marginTop: '25px'}}>
            <Field name="_type">
              {props => (
                <div>
                  <Select placeholder="Choose your type" options={credentialOptions} isMulti={false} onChange={props.input.onChange} defaultValue={{ value: defaultSelectValue, label: "Choose vendor" }} />
                </div>
              )}
            </Field>
          </div>
          <div>
            {
              values._type.value === "smb" && SmbCredentials.map((row: any) => (
                  <Field name={row.name} key={row.id}>
                    {props => (
                      <TextField
                        id={row.id}
                        type={row.type}
                        label={row.placeholder}
                        placeholder={row.placeholder}
                        onChange={props.input.onChange}
                        margin="normal"
                      />
                    )}
                  </Field>
                ))
            }
            {
                values._type.value === "mail" && MailCredentials.map((row: any) => (
                  <Field name={row.name} key={row.id}>
                    {props => (
                      <TextField
                        id={row.id}
                        type={row.type}
                        label={row.placeholder}
                        placeholder={row.placeholder}
                        onChange={props.input.onChange}
                        margin="normal"
                      />
                    )}
                  </Field>
                ))
            }
            {
                values._type.value === "ftp" && FtpCredentials.map((row: any) => (
                  <Field name={row.name} key={row.id}>
                    {props => (
                      <TextField
                        id={row.id}
                        type={row.type}
                        label={row.placeholder}
                        placeholder={row.placeholder}
                        onChange={props.input.onChange}
                        margin="normal"
                      />
                    )}
                  </Field>
                ))
            }            
          </div>
          <div className="buttons">
            <Button variant="outline" color="error" onClick={form.reset} disabled={pristine}>
                {t('editadd.reset')}
            </Button>
            <button className="custom-button-success" type="submit" disabled={submitting || pristine}>
                {t('editadd.reset')}
            </button>
          </div>
        </form>
      )}
    />
    <Toast
      variant={toast.variant}
      open={toast.isOpen}
      header={toast.header}
      message={toast.message}
    />
    </div>
  );
};

export default NewJob;