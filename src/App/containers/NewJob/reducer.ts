import {
  CREATE_JOB_REQUEST,
  CREATE_JOB_SUCCESS,
  CREATE_JOB_ERROR
} from "./constants";
import { IInitialState } from "./model";
import { IAction } from "../../../models";

export const initialState: IInitialState = {
  error: {},
  data: {},
  loading: false,
};

export const getErrorSelector = (state: any) => state.onejob.error;
export const getLoadingSelector = (state: any) => state.onejob.loading;

export default (state = initialState, action: IAction) => {
  switch (action.type) {
    case CREATE_JOB_REQUEST:
      return {
        ...state,
        loading: true
      };

    case CREATE_JOB_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false
      };

    case CREATE_JOB_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };

    default:
      return state;
  }
};
