export const CREATE_JOB_REQUEST: string = "CREATE_JOB_REQUEST";
export const CREATE_JOB_SUCCESS: string = "CREATE_JOB_SUCCESS";
export const CREATE_JOB_ERROR: string = "CREATE_JOB_ERROR";

export const pmsOptions = [
    { value: "ubsofthotel50", label: "ubsofthotel50" },
    { value: "citadelhotelsystemdesk", label: "citadelhotelsystemdesk" },
    { value: "asahotel", label: "asahotel" },
    { value: "oraclefideliosuite8", label: "oraclefideliosuite8" }
]

export let credentialOptions = [
    { value: "mail", label: "Mail" },
    { value: "smb", label: "SMB" },
    { value: "ftp", label: "FTP" }
  ]

export let MailCredentials = [
    {name: "username", id: "username", type: "text", placeholder: "Username", value: ""},
    {name: "password", id: "password", type: "password", placeholder: "Password", value: ""},
    {name: "protocol", id: "protocol", type: "text", placeholder: "Protocol", value: ""},
    {name: "host", id: "host", type: "text", placeholder: "Host", value: ""},
    {name: "port", id: "port", type: "number", placeholder: "Port", value: ""}
]

export let SmbCredentials = [
    {name: "username", id: "username", type: "text", placeholder: "Username", value: ""},
    {name: "password", id: "password", type: "password", placeholder: "Password", value: ""},
    {name: "host", id: "host", type: "text", placeholder: "Host", value: ""},
    {name: "port", id: "port", type: "number", placeholder: "Port", value: ""},
    {name: "domain", id: "domain", type: "text", placeholder: "Domain", value: ""},
    {name: "path", id: "path", type: "text", placeholder: "Path", value: ""},
    {name: "share", id: "share", type: "text", placeholder: "Share", value: ""}
]

export let FtpCredentials = [
    {name: "username", id: "username", type: "text", placeholder: "Username", value: ""},
    {name: "password", id: "password", type: "password", placeholder: "Password", value: ""},
    {name: "protocol", id: "protocol", type: "text", placeholder: "Protocol", value: ""},
    {name: "host", id: "host", type: "text", placeholder: "Host", value: ""},
    {name: "port", id: "port", type: "number", placeholder: "Port", value: ""},
    {name: "path", id: "path", type: "text", placeholder: "Path", value: ""},
]