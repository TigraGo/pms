import { connect } from "react-redux";
import { getLoadingSelector, getErrorSelector } from "./reducer";
import { sendNewJob } from './thunx';
import NewJob from "./component";
import { getToastSelector } from "../../utils/Toast/reducer";

const mapStateToProps = (state: any) => ({
  loading: getLoadingSelector(state),
  error: getErrorSelector(state),
  toast: getToastSelector(state)
});

export default connect(
  mapStateToProps,
  { sendNewJob }
)(NewJob);
