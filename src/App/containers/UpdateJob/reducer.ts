import {
  UPDATE_JOB_REQUEST,
  UPDATE_JOB_SUCCESS,
  UPDATE_JOB_ERROR,
} from "./constants";
import { IInitialState } from "./model";
import { IAction } from "../../../models";

export const initialState: IInitialState = {
  loading: false,
  success: false,
};

export const getSuccessSelector = (state: any) => state.updatejob.success;
export const getErrorSelector = (state: any) => state.updatejob.error;
export const getLoadingSelector = (state: any) => state.updatejob.loading;

export default (state = initialState, action: IAction) => {
  switch (action.type) {
    case UPDATE_JOB_REQUEST:
      return {
        ...state,
        loading: true
      };

    case UPDATE_JOB_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true
      };

    case UPDATE_JOB_ERROR:
      return {
        ...state,
        loading: false,
        success: false
      };

    default:
      return state;
  }
};
