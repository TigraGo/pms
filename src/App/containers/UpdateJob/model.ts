export interface IInitialState {
  loading: boolean;
  success: boolean;
}

export interface IProps {
  data: any;
  loading: boolean;
  toast: any;
  updateJob: (body: any, jobId: string) => void;
}

export interface IResponse {
  body: string;
}