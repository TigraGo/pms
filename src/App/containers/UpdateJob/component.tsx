import React, { FC, useState } from "react";
import {Form, Field} from "react-final-form";
import { useParams } from 'react-router-dom';
import { Loader, Select, TextField, Button, Toast } from "@betterspace/react-component-suite";
import { pmsOptions, credentialOptions, SmbCredentials, MailCredentials, FtpCredentials } from "../NewJob/constants";

import { IProps } from "./model";
import "./style.scss";
import { useTranslation } from "react-i18next";

const UpdateJob: FC<IProps> = ({
  loading,
  updateJob,
  data,
  toast
}) => {

  const [defaultCredentialsValue] = useState(data.type);
  const [defaultVendorsValue] = useState(data.credentials._type);
  const [defaultIdValue] = useState(data.id);

  let { id } = useParams();
  const { t } = useTranslation();

  const onSubmit = async (values: any) => {
    let credentials: any = JSON.parse(JSON.stringify(values)); // clone new object for setting in 'credentials'
    credentials['_type'] = values._type.value || values._type;

    values['site'] = credentials.site.value;

    delete credentials['type']; // removing unnecessary properties
    
    const body = { // summary object for request
      type: values.type.value,
      site: credentials.site.value,
      credentials
    }
    
    updateJob(JSON.stringify(body), `${id}`);
  }

  if (loading || !data) {
    return (
      <div className="loading">
        <Loader width="25%" />
      </div>
    );
  }

  return (
    <div className="jobslist create-job">
      <Form
      onSubmit={onSubmit}
      initialValues={Object.assign({ type: {value: defaultCredentialsValue }, _type: {value: defaultVendorsValue}, site: {value: defaultIdValue}}, data.credentials)}
      render={({ handleSubmit, form, submitting, pristine, values }) => (
        <form onSubmit={handleSubmit}>
          <h2>{t('editadd.choosecredentialstype')}</h2>
          <div>
            <Field name="type">
              {props => (
                <div>
                  <Select placeholder="Choose type" options={pmsOptions} isMulti={false} onChange={props.input.onChange} defaultValue={{ value: defaultCredentialsValue, label: defaultCredentialsValue}} />
                </div>
              )}
            </Field>
          </div>
          <div>
            <Field name="site">
              {props => (
                <TextField
                  id="site"
                  label="Site"
                  placeholder="Site"
                  onChange={props.input.onChange}
                  margin="normal"
                  defaultValue={data.id}
                />
              )}
            </Field>
          </div>
          <div style={{marginTop: '25px'}}>
            <Field name="_type">
              {props => (
                <div>
                  <Select placeholder="Choose your type" options={credentialOptions} isMulti={false} onChange={props.input.onChange} defaultValue={{ value: defaultVendorsValue, label: defaultVendorsValue }} />
                </div>
              )}
            </Field>
          </div>
          <div>
          {
            (values._type.value === "smb" || values._type === 'smb') && SmbCredentials.map((row: any) => (
              <Field name={row.name} key={row.id}>
                {props => (
                  <TextField
                    id={row.id}
                    type={row.type}
                    label={row.placeholder}
                    placeholder={row.placeholder}
                    onChange={props.input.onChange}
                    margin="normal"
                    defaultValue={data.credentials[row.id]}
                  />
                )}
              </Field>
            ))
          }
          {
            (values._type.value === "ftp" || values._type === "ftp") && FtpCredentials.map((row: any) => (
              <Field name={row.name} key={row.id}>
                {props => (
                  <TextField
                    id={row.id}
                    type={row.type}
                    label={row.placeholder}
                    placeholder={row.placeholder}
                    onChange={props.input.onChange}
                    margin="normal"
                    defaultValue={data.credentials[row.id]}
                  />
                )}
              </Field>
            ))
          }
          {
            (values._type.value === "mail" || values._type === "mail") && MailCredentials.map((row: any) => (
              <Field name={row.name} key={row.id}>
                {props => (
                  <TextField
                    id={row.id}
                    type={row.type}
                    label={row.placeholder}
                    placeholder={row.placeholder}
                    onChange={props.input.onChange}
                    margin="normal"
                    defaultValue={data.credentials[row.id]}
                  />
                )}
              </Field>
            ))
          }
          </div>
          <div className="buttons">
            <Button variant="outline" color="error" onClick={form.reset} disabled={pristine}>
              {t('editadd.reset')}
            </Button>
            <button className="custom-button-success" type="submit" disabled={submitting || pristine}>
              {t('editadd.submit')}
            </button>
          </div>
        </form>
      )}
    />
    <Toast
      variant={toast.variant}
      open={toast.isOpen}
      header={toast.header}
      message={toast.message}
    />
    </div>
  );
};

export default UpdateJob;