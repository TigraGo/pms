import {successToast, errorToast} from '../../utils/Toast/toast';

import {
  updateJobError,
  updateJobRequest,
  updateJobSuccess,
} from "./actions";

import { API, api } from "../../../services/api";

export const updateJob: any = (body: any, jobId: string) => async (dispatch: any) => {
  dispatch(updateJobRequest());
  try {
    const response = await api(`${API.JOBS}/${jobId}`, 'PUT', body);

    if (response.status < 300) {
      dispatch(updateJobSuccess());
      dispatch(successToast('UPDATE JOB', 'Job was successfully updated'));
      return;
    }

    dispatch(updateJobError());
    dispatch(errorToast('UPDATE JOB', `Error was found with status ${response.status}`));

  } catch (error) {
    dispatch(updateJobError(error));
    dispatch(errorToast('UPDATE JOB', 'Error was found'));
  }
};
