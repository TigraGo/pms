export const UPDATE_JOB_REQUEST: string = "UPDATE_JOB_REQUEST";
export const UPDATE_JOB_SUCCESS: string = "UPDATE_JOB_SUCCESS";
export const UPDATE_JOB_ERROR: string = "UPDATE_JOB_ERROR";
export const SET_TOAST: string = "SET_TOAST";

export const pmsOptions = [
    { value: "ubsofthotel50", label: "ubsofthotel50" },
    { value: "citadelhotelsystemdesk", label: "citadelhotelsystemdesk" },
    { value: "asahotel", label: "asahotel" },
    { value: "oraclefideliosuite8", label: "oraclefideliosuite8" }
]

export const credentialOptions = [
    { value: "mail", label: "Mail" },
    { value: "smb", label: "SMB" },
    { value: "ftp", label: "FTP" }
  ]

export const MailCredentials = [
    {name: "username", id: "username", type: "text", placeholder: "Username"},
    {name: "password", id: "password", type: "password", placeholder: "Password"},
    {name: "protocol", id: "protocol", type: "text", placeholder: "Protocol"},
    {name: "host", id: "host", type: "text", placeholder: "Host"},
    {name: "port", id: "port", type: "number", placeholder: "Port"}
]

export const SmbCredentials = [
    {name: "username", id: "username", type: "text", placeholder: "Username"},
    {name: "password", id: "password", type: "password", placeholder: "Password"},
    {name: "host", id: "host", type: "text", placeholder: "Host"},
    {name: "port", id: "port", type: "number", placeholder: "Port"},
    {name: "domain", id: "domain", type: "text", placeholder: "Domain"},
    {name: "path", id: "path", type: "text", placeholder: "Path"},
    {name: "share", id: "share", type: "text", placeholder: "Share"}
]

export const FtpCredentials = [
    {name: "username", id: "username", type: "text", placeholder: "Username"},
    {name: "password", id: "password", type: "password", placeholder: "Password"},
    {name: "protocol", id: "protocol", type: "text", placeholder: "Protocol"},
    {name: "host", id: "host", type: "text", placeholder: "Host"},
    {name: "port", id: "port", type: "number", placeholder: "Port"},
    {name: "path", id: "path", type: "text", placeholder: "Path"},
]