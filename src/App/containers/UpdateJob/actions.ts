import {
  UPDATE_JOB_ERROR,
  UPDATE_JOB_REQUEST,
  UPDATE_JOB_SUCCESS,
  SET_TOAST
} from "./constants";
import { IAction } from "../../../models";
import { ActionCreator } from "redux";

export const updateJobRequest: ActionCreator<IAction> = () => ({
  type: UPDATE_JOB_REQUEST
});

export const updateJobSuccess: ActionCreator<IAction> = () => ({
  type: UPDATE_JOB_SUCCESS,
});

export const updateJobError: ActionCreator<IAction> = () => ({
  type: UPDATE_JOB_ERROR,
});

export const setToast: ActionCreator<IAction> = payload => ({
  type: SET_TOAST,
  payload
});