import { connect } from "react-redux";
import { getSuccessSelector, getLoadingSelector } from "./reducer";
import { updateJob } from './thunx';
import NewJob from "./component";
import { getDataSelector } from "../OneJob/reducer";
import { getToastSelector } from "../../utils/Toast/reducer";

const mapStateToProps = (state: any) => ({
  data: getDataSelector(state),
  success: getSuccessSelector(state),
  loading: getLoadingSelector(state),
  toast: getToastSelector(state)
});

export default connect(
  mapStateToProps,
  { updateJob }
)(NewJob);
