import React, { FC, useEffect, useState } from "react";
import { useParams, Link } from 'react-router-dom';

import Tooltip from "@material-ui/core/Tooltip";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from "@date-io/date-fns";
import { useTranslation } from "react-i18next";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlayCircle, faCheckCircle, faTimesCircle, faEdit, faTrashAlt, faFile } from '@fortawesome/free-solid-svg-icons'

import { Loader, Button, Toast, Select, TextField } from '@betterspace/react-component-suite';


import { SELECT_OPTIONS } from './constants';

import { IProps } from "./model";
import "./style.scss";

const OneJob: FC<IProps> = ({
  executions,
  data,
  executionFiles,
  loading,
  getJobData,
  removeJob,
  getExecutions,
  getExecutionFiles,
  toast
}) => {

  let { id } = useParams();
  let [executionsSelectValue, setSelectValue] = useState('');
  const [executionsStartValue, setStartValue] = useState(new Date(Date.now() - 604800000));
  const [executionsEndValue, setEndValue] = useState(new Date());
  const [expanded, setExpanded] = React.useState<string | false>(false);

  let executionsBody = {
    status: executionsSelectValue,
    start: executionsStartValue.toISOString(),
    end: executionsEndValue.toISOString()
  }

  const handleChange = (panel: string) => (event: React.ChangeEvent<{}>, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : false);
  };

  const { t } = useTranslation();

  useEffect(() => {
    getJobData(`${id}`); // send ID of the job to our thunx-func
    getExecutions(`${id}`);
  }, [getJobData, getExecutions, getExecutionFiles, id]); // add dependency for useEffect hoooks

  if (loading || !executions || !data) {
    return (
      <div className="loading">
        <Loader width="25%" />
      </div>
    );
  }



  return (
    <div className="jobslist">
      <div className="jobslist__inner one-job">
        <div className="jobslist__header">
          {data && ( // taking data from the jobs list
            <div>
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <h2>
                        {data ? data.type : null}
                      </h2>
                    </TableCell>
                    <TableCell>
                      <h2 className="credentials-row">
                        <Link to={`/pms/jobs/${id}/edit`}>
                          <FontAwesomeIcon className="icon icon-progress" icon={faEdit} />
                        </Link>
                        <FontAwesomeIcon onClick={() => { removeJob(data.id) }} className="icon icon-progress" icon={faTrashAlt} />
                      </h2>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
              <div className="jobslist__row">
                <span>JobID</span>
                {data.id}
              </div>
              <div className="jobslist__row">
                <span>Vendor</span>
                {data.type}
              </div>

              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell>
                      <h2>
                        {t('onejob.credentials')}
                      </h2>
                    </TableCell>
                    <TableCell>
                      <h2 className="credentials-row" style={{ textTransform: 'uppercase', fontWeight: 'normal' }}>
                        {data.credentials ? data.credentials._type : null}
                      </h2>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>

              {
                data.credentials ? Object.entries(data.credentials).map(([key, value]) => { // transforming object to array and map credentials dynamically
                  return (
                    <div className="jobslist__row" id={key} key={key}>
                      <span>{t(`onejob.${key}`)}</span>
                      <TextField
                        type={key === 'password' ? 'password' : 'text'}
                        margin="normal"
                        value={value}
                      />
                    </div>
                  );
                }) : null
              }
            </div>
          )}
        </div>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell>
                <h2>
                  {t('onejob.executions')}
                </h2>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
        <div className="executions__filter">
          <div className="executions__filter-item">
            <Select
              options={SELECT_OPTIONS}
              isMulti={false}
              placeholder={t('onejob.selectstatus')}
              onChange={(e: any) => { setSelectValue(e.value) }}
            />
          </div>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <div className="executions__filter-item">
              <div className="">
                <KeyboardDatePicker
                  autoOk
                  label="Start Date"
                  clearable
                  disableFuture
                  value={executionsStartValue}
                  onChange={(e: any) => { setStartValue(e) }}
                />
              </div>
            </div>
            <div className="executions__filter-item">
              <div className="">
                <KeyboardDatePicker
                  autoOk
                  label="End Date"
                  disableFuture
                  value={executionsEndValue}
                  onChange={(e: any) => { setEndValue(e) }}
                />
              </div>
            </div>
          </MuiPickersUtilsProvider>
        </div>
        <div style={{ textAlign: 'right' }}>
          <Button
            variant="outline"
            color="primary"
            onClick={() => { getExecutions(`${id}`, executionsBody) }}
            id="filter-button">
            Filter
          </Button>
        </div>
        {
          (executions.length !== 0) ? (
              <div className="executions__inner">
              {executions.map((row: any) => (
                <div key={row.id}>
                  <ExpansionPanel expanded={expanded === row.id} onClick={() => {getExecutionFiles()}} onChange={handleChange(row.id)}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1bh-content"  
                        id="panel1bh-header"
                      >
                      {
                        row.status === 'IN_PROGRESS' &&
                        <Tooltip title="In Progress" placement="top-start">
                          <div>
                            <FontAwesomeIcon className="icon icon-progress" icon={faPlayCircle} />
                          </div>
                        </Tooltip>
                      }
                      {row.status === 'SUCCESS' &&
                        <Tooltip title="Success" placement="top-start">
                          <div>
                            <FontAwesomeIcon className="icon icon-success" icon={faCheckCircle} />
                          </div>
                        </Tooltip>
                      }
                      {row.status === 'ERROR' &&
                        <Tooltip title="Error" placement="top-start">
                          <div>
                            <FontAwesomeIcon className="icon icon-error" icon={faTimesCircle} />
                          </div>
                        </Tooltip>
                      }
                      <div>{row.start.replace(/T|Z/g, ' ').slice(0, 19)}</div>
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails>
                      <div className="executions__row">
                        <h3 className="executions__row-item">
                          {t('onejob.enddate')}
                        </h3>
                        <div className="executions__row-item">
                          {row.end === undefined ? 'In progress' : row.end.replace(/T|Z/g, ' ').slice(0, 19)}
                        </div>
                      </div>
                      <div className="executions__row">
                        <h3 className="executions__row-item">
                          {t('onejob.errormessage')}
                        </h3>
                        <div className="executions__row-item">
                          {t('onejob.none')}
                        </div>
                      </div>
                      <div className="executions__row" style={{width: '100%'}}>
                        <h3 className="executions__row-item">
                          {t('onejob.files')}
                        </h3>
                      </div>
                      <div className="executions__row">
                      {
                          executionFiles.length > 0 ? executionFiles.map((item: any) => (
                            <div key={item.id} className="files-list__item">
                              <a href={`/api/v1/ingest/jobs/${id}/executions/${row.id}/files/${item.id}`} download={true}>
                                <FontAwesomeIcon className="icon icon-file" icon={faFile} />
                                <div>{item.name}</div>
                              </a>
                            </div>
                          )) : null
                        }
                      </div>
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  </div>
                  ))}
              </div>
          ) : <Paper className="jobslist__wrapper">
              <Table className="jobslist__table no-executions" aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell align="left">{t('onejob.status')}</TableCell>
                    <TableCell align="left">{t('onejob.startdate')}</TableCell>
                    <TableCell align="left">{t('onejob.enddate')}</TableCell>
                  </TableRow>
                </TableHead>
              </Table>
              <div className="handler-text">{t('onejob.noexecutions')}</div>
            </Paper>
        }
      </div>
      <Toast
        variant={toast.variant}
        open={toast.isOpen}
        header={toast.header}
        message={toast.message}
      />
    </div>
  );
};

export default OneJob;
