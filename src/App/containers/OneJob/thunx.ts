import { Dispatch } from "redux";

import {
  executionsRequest,
  executionsSuccess,
  executionsError,
  credentialsError,
  credentialsRequest,
  credentialsSuccess,
  executionFilesRequest,
  executionFilesSuccess,
  executionFilesError
} from "./actions";
import { API, api } from "../../../services/api";

export const getJobData: any = (jobId: string) => async (dispatch: Dispatch) => {
  dispatch(credentialsRequest());
  dispatch(executionsRequest());
  try {
    const credentialsResponse = await api(`${API.JOBS}/${jobId}`, 'GET');
    const credentialsResult = await credentialsResponse.json();
    dispatch(credentialsSuccess(credentialsResult))
  } catch (error) {
    dispatch(credentialsError(error));
  }
};

export const getExecutions: any = (jobId: string, body?: any) => async (dispatch: Dispatch) => {
  dispatch(executionsRequest());
  try {
    var serialized = "";
    for (var key in body) {
      if (serialized !== "") {
        serialized += "&";
      }
      serialized += key + "=" + encodeURIComponent(body[key]);
    }
    const executionsResponse = await api(`${API.JOBS}/${jobId}/executions?${serialized}`, 'GET');
    const executionsResult = await executionsResponse.json();
    dispatch(executionsSuccess(executionsResult));
  } catch (error) {
    dispatch(executionsError(error));
  }
}

export const getExecutionFiles: any = (executionId?: any) => async (dispatch: Dispatch) => {
  dispatch(executionFilesRequest());
  try {
    const executionFilesResponse = await fetch(`https://jsonplaceholder.typicode.com/users`);
    const executionFilesResult = await executionFilesResponse.json();
    dispatch(executionFilesSuccess(executionFilesResult));
  } catch (error) {
    dispatch(executionFilesError(error));
  }
}