import {
  CREDENTIALS_ERROR,
  CREDENTIALS_REQUEST,
  CREDENTIALS_SUCCESS,
  EXECUTIONS_REQUEST,
  EXECUTIONS_SUCCESS,
  EXECUTIONS_ERROR,
  EXECUTIONS_FILES_REQUEST,
  EXECUTIONS_FILES_SUCCESS,
  EXECUTIONS_FILES_ERROR
} from "./constants";
import { IAction } from "../../../models";
import { ActionCreator } from "redux";

export const credentialsRequest: ActionCreator<IAction> = () => ({
  type: CREDENTIALS_REQUEST
});

export const credentialsSuccess: ActionCreator<IAction> = payload => ({
  type: CREDENTIALS_SUCCESS,
  payload
});

export const credentialsError: ActionCreator<IAction> = payload => ({
  type: CREDENTIALS_ERROR,
  payload
});

export const executionsRequest: ActionCreator<IAction> = () => ({
  type: EXECUTIONS_REQUEST,
});

export const executionsSuccess: ActionCreator<IAction> = payload => ({
  type: EXECUTIONS_SUCCESS,
  payload
});

export const executionsError: ActionCreator<IAction> = payload => ({
  type: EXECUTIONS_ERROR,
  payload
});

export const executionFilesRequest: ActionCreator<IAction> = () => ({
  type: EXECUTIONS_FILES_REQUEST,
});

export const executionFilesSuccess: ActionCreator<IAction> = payload => ({
  type: EXECUTIONS_FILES_SUCCESS,
  payload
});

export const executionFilesError: ActionCreator<IAction> = payload => ({
  type: EXECUTIONS_FILES_ERROR,
  payload
});