import {
  CREDENTIALS_REQUEST,
  CREDENTIALS_SUCCESS,
  CREDENTIALS_ERROR,
  EXECUTIONS_ERROR,
  EXECUTIONS_REQUEST,
  EXECUTIONS_SUCCESS,
  EXECUTIONS_FILES_REQUEST,
  EXECUTIONS_FILES_SUCCESS,
  EXECUTIONS_FILES_ERROR
} from "./constants";
import { IInitialState } from "./model";
import { IAction } from "../../../models";

export const initialState: IInitialState = {
  executions: [],
  executionFiles: {},
  data: {},
  error: "",
  loading: false,
};

export const getDataSelector = (state: any) => state.onejob.data;
export const getJobsListSelector = (state: any) => state.onejob.jobslist;
export const getExecutionsSelector = (state: any) => state.onejob.executions;

export const getErrorSelector = (state: any) => state.onejob.error;
export const getLoadingSelector = (state: any) => state.onejob.loading;

export const getExecutionFilesSelector = (state: any) => state.onejob.executionFiles;

export default (state = initialState, action: IAction) => {
  switch (action.type) {
    case CREDENTIALS_REQUEST:
      return {
        ...state,
        loading: true
      };

    case CREDENTIALS_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false
      };

    case CREDENTIALS_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };

    case EXECUTIONS_REQUEST:
      return {
        ...state,
      };

    case EXECUTIONS_SUCCESS:
      return {
        ...state,
        executions: action.payload,
        loading: false
      };

    case EXECUTIONS_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };

    case EXECUTIONS_FILES_REQUEST:
      return {
        ...state,
      };

    case EXECUTIONS_FILES_SUCCESS:
      return {
        ...state,
        executionFiles: action.payload,
      };

    case EXECUTIONS_FILES_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
