export interface IInitialState {
  executions: [];
  executionFiles: {};
  data: any;
  error: string;
  loading: boolean;
}

export interface IProps {
  executions: any;
  jobslist: any;
  executionFiles: any;
  data: any;
  credentials?: any;
  loading: boolean;
  toast: any;
  getJobData: (jobId: string) => void;
  removeJob: (jobId: string) => void;
  getExecutions: (jobId: string, body?: any) => void;
  getExecutionFiles: (jobId?: string) => void;
}

export interface IResponse {
  body: string;
  id: number;
  title: string;
  userId: number;
}