import { connect } from "react-redux";
import { removeJob } from '../JobsList/thunx';
import { getJobData, getExecutions, getExecutionFiles } from "./thunx";
import { getExecutionsSelector, getLoadingSelector, getDataSelector, getExecutionFilesSelector } from "./reducer";
import { getToastSelector } from "../../utils/Toast/reducer";
import OneJob from "./component";

const mapStateToProps = (state: any) => ({
  executions: getExecutionsSelector(state),
  data: getDataSelector(state),
  loading: getLoadingSelector(state),
  executionFiles: getExecutionFilesSelector(state),
  toast: getToastSelector(state)
});

export default connect(
  mapStateToProps,
  { getJobData, removeJob, getExecutions, getExecutionFiles }
)(OneJob);
