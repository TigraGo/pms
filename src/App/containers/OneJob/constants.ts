export const CREDENTIALS_REQUEST: string = "CREDENTIALS_REQUEST";
export const CREDENTIALS_SUCCESS: string = "CREDENTIALS_SUCCESS";
export const CREDENTIALS_ERROR: string = "CREDENTIALS_ERROR";
export const EXECUTIONS_REQUEST: string = "EXECUTIONS_REQUEST";
export const EXECUTIONS_SUCCESS: string = "EXECUTIONS_SUCCESS";
export const EXECUTIONS_ERROR: string = "EXECUTIONS_ERROR";
export const EXECUTIONS_FILES_REQUEST: string = "EXECUTIONS_FILES_REQUEST";
export const EXECUTIONS_FILES_SUCCESS: string = "EXECUTIONS_FILES_SUCCESS";
export const EXECUTIONS_FILES_ERROR: string = "EXECUTIONS_FILES_ERROR";

export const SELECT_OPTIONS: any = [
    { value: '', label: 'All' },
    { value: 'SUCCESS', label: 'Success' },
    { value: 'IN_PROGRESS', label: 'In progress' },
    { value: 'ERROR', label: 'Error' }
]

export const MOCKED_FILES_LIST: any = [
    { label: 'File1', link: 'https://fb.com' },
    { label: 'File2', link: 'https://vk.com' },
    { label: 'File3', link: 'https://google.com' }
]