import { connect } from "react-redux";
import { getJobsListData, removeJob } from "./thunx";
import { getDataSelector, getLoadingSelector } from "./reducer";
import JobsList from "./component";
import { getToastSelector } from "../../utils/Toast/reducer";

const mapStateToProps = (state: any) => ({
  data: getDataSelector(state),
  loading: getLoadingSelector(state),
  toast: getToastSelector(state)
});

export default connect(
  mapStateToProps,
  { getJobsListData, removeJob }
)(JobsList);
