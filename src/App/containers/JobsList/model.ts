export interface IInitialState {
  data: any;
  error: string;
  loading: boolean;
}

export interface IProps {
  data: any;
  loading: boolean;
  toast: any;
  getJobsListData: () => void;
  removeJob: (jobId: string) => void;
}

export interface IResponse {
  body: string;
  id: number;
  title: string;
  userId: number;
}