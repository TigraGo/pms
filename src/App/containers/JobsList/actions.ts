import {
  JOBS_LIST_ERROR,
  JOBS_LIST_REQUEST,
  JOBS_LIST_SUCCESS,
  REMOVE_JOB_ERROR,
  REMOVE_JOB_REQUEST,
  REMOVE_JOB_SUCCESS
} from "./constants";
import { IAction } from "../../../models";
import { ActionCreator } from "redux";

export const getJobsListRequest: ActionCreator<IAction> = () => ({
  type: JOBS_LIST_REQUEST
});

export const getJobsListSuccess: ActionCreator<IAction> = payload => ({
  type: JOBS_LIST_SUCCESS,
  payload,
});

export const getJobsListError: ActionCreator<IAction> = payload => ({
  type: JOBS_LIST_ERROR,
  payload
});

export const removeJobRequest: ActionCreator<IAction> = () => ({
  type: REMOVE_JOB_REQUEST,
});

export const removeJobError: ActionCreator<IAction> = payload => ({
  type: REMOVE_JOB_ERROR,
  payload
});

export const removeJobSuccess: ActionCreator<IAction> = () => ({
  type: REMOVE_JOB_SUCCESS,
});