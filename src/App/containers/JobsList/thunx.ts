import { Dispatch } from "redux";
import { successToast, errorToast } from '../../utils/Toast/toast';

import {
  getJobsListError,
  getJobsListRequest,
  getJobsListSuccess,
  removeJobRequest,
  removeJobError,
  removeJobSuccess
} from "./actions";

import { API, api } from "../../../services/api";

export const getJobsListData: any = () => async (dispatch: Dispatch) => {
  dispatch(getJobsListRequest());
  try {
    const response = await api(API.JOBS, 'GET');
    const result = await response.json();
    dispatch(getJobsListSuccess(result));

  } catch (error) {
    dispatch(getJobsListError(error));
  }
};

export const removeJob: any = (jobId: string) => async (dispatch: any) => {
  dispatch(removeJobRequest());
  try {
    await api(`${API.JOBS}/${jobId}`, 'DELETE');
    dispatch(removeJobSuccess());
    dispatch(successToast('DELETE JOB', 'Job was successfully deleted'));

  } catch (error) {
    dispatch(removeJobError(error));
    dispatch(errorToast('DELETE JOB', 'Error was found'));
  }
}
