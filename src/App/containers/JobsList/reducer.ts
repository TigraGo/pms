import {
  JOBS_LIST_REQUEST,
  JOBS_LIST_SUCCESS,
  JOBS_LIST_ERROR
} from "./constants";
import { IInitialState } from "./model";
import { IAction } from "../../../models";

export const initialState: IInitialState = {
  data: [],
  error: "",
  loading: false,
};

export const getDataSelector = (state: any) => state.jobslist.data;
export const getErrorSelector = (state: any) => state.jobslist.error;
export const getLoadingSelector = (state: any) => state.jobslist.loading;

export default (state = initialState, action: IAction) => {
  switch (action.type) {
    case JOBS_LIST_REQUEST:
      return {
        ...state,
        loading: true
      };

    case JOBS_LIST_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false
      };

    case JOBS_LIST_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };

    default:
      return state;
  }
};
