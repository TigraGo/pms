import React, { FC, useEffect } from "react";
import { Link } from 'react-router-dom';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { useTranslation } from "react-i18next";

import { Loader, Toast } from '@betterspace/react-component-suite';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'

import { IProps } from "./model";
import "./style.scss";

const JobsList: FC<IProps> = ({
  getJobsListData,
  removeJob,
  data,
  loading,
  toast
}) => {
  useEffect(() => {
    getJobsListData();
  }, [getJobsListData]);

  const { t } = useTranslation();

  if (loading) {
    return (
      <div className="loading">
        <Loader width="25%" />
      </div>
    );
  }

  return (
    <div className="jobslist">
      <div className="jobslist__inner">
        <Paper className="jobslist__wrapper">
          <Table className="jobslist__table" aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="left">ID</TableCell>
                <TableCell align="left">{t('listjob.type')}</TableCell>
                <TableCell align="left">{t('listjob.site')}</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data && data.map((row: any) => (
                <TableRow key={row.id}>
                  <Link to={`jobs/${row.id}`}>
                    <TableCell align="left">{row.id}</TableCell>
                    <TableCell align="left">{row.type}</TableCell>
                    <TableCell align="left">{row.site}</TableCell>
                  </Link>
                    <TableCell align="left">
                      <FontAwesomeIcon onClick={() => {removeJob(row.id)}} className="icon icon-progress" icon={faTrashAlt} />
                    </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          {
            data.length === 0 && (
            <div className="handler-text">{t('listjob.nojobsyet')}</div> 
            )
          }
        </Paper>
        <Link className="jobslist__new-job" to ='/pms/jobs/new'>{t('listjob.createnewjob')}</Link>
      </div>
      <Toast
        variant={toast.variant}
        open={toast.isOpen}
        header={toast.header}
        message={toast.message}
      />
    </div>
  );
};

export default JobsList;
