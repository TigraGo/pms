export const JOBS_LIST_REQUEST: string = "JOBS_LIST_REQUEST";
export const JOBS_LIST_SUCCESS: string = "JOBS_LIST_SUCCESS";
export const JOBS_LIST_ERROR: string = "JOBS_LIST_ERROR";
export const REMOVE_JOB_REQUEST: string = "REMOVE_JOB_REQUEST";
export const REMOVE_JOB_ERROR: string = "REMOVE_JOB_ERROR";
export const REMOVE_JOB_SUCCESS: string = "REMOVE_JOB_SUCCESS";
