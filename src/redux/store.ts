import { applyMiddleware, createStore } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./rootReducer";

export const configureStore = (initialState = {}) => {
  let middleware = applyMiddleware(thunkMiddleware);

  if (process.env.NODE_ENV !== "production") {
    middleware = composeWithDevTools(middleware);
  }

  return createStore(rootReducer, initialState, middleware);
};