import { combineReducers, AnyAction, Reducer } from "redux";
import jobslist from '../App/containers/JobsList/reducer';
import onejob from '../App/containers/OneJob/reducer';
import newjob from '../App/containers/NewJob/reducer';
import updatejob from '../App/containers/UpdateJob/reducer';
// import oneexecution from '../App/containers/OneExecution/reducer';
import toast from '../App/utils/Toast/reducer';

const appReducer: Reducer = combineReducers({
  jobslist,
  onejob,
  newjob,
  updatejob,
  // oneexecution,
  toast
});

export default (state: any, action = {}) =>
  appReducer(state, action as AnyAction);
