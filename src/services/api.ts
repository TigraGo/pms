interface IAPI {
  JOBS: string,
}

const BASE_URL: string = '/api/v1/ingest';

export const API: IAPI = {
  JOBS: `/jobs`
};

let tokenData: any = localStorage.getItem('tokens-betterspace360');
let token = JSON.parse(tokenData)[0].access_token;

export const api = (url: string, type: string, body?: any) => {
  return fetch(`${BASE_URL}${url}`, {
    method: type,             // 'GET', 'PUT', 'DELETE', etc.
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: body // object with data for POST/PUT requests, etc. NOT REQUIRED
  })
}