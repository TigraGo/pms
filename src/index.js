import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import Routes from './App/routes';
import { configureStore } from './redux/store';

import singleSpaReact from 'single-spa-react';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'

import './App/utils/i18n/i18n';

const appName = 'pms'; // TODO: change to your apps name!

const root = () => {
  const store = configureStore();
  return (
    <Provider store={store}>
      <Router>
        <Routes />
      </Router>
    </Provider>
  );
};

const reactLifecycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: root,
  domElementGetter: loadApp,
});

export const bootstrap = reactLifecycles.bootstrap;
export const mount = reactLifecycles.mount;
export const unmount = reactLifecycles.unmount;

function loadApp() {
  let mainContent = document.getElementById('main_content'); // defined by UI frame
  let app = document.getElementById(appName);
  if (!app) {
    app = document.createElement('div');
    app.id = appName;
    mainContent.appendChild(app);
  }
  return app;
}